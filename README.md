# Tài liệu học tập Kubernets

## Khái quát chung về kubernets
 `Kubernets` là một mã nguồn mở được dùng để triển khai hệ thống tự động, scaling, quản lý các containers. Được phát triển bởi goole (hiện gg dùng kubernets quản lý hàng tỉ các containers).

## Những kiến thức cần có
- [Tài liệu Docker](./knowledge/Docker.md)
- [Tài liệu về Network]()


## Mô hình triển khai

### Khi nào thì nên triển khai kubernets
- Với hệ thống nhiều máy chủ vật lý (trên 3 server vật lý)
- Sử dụng container engine
- Hệ thống triển khai theo mô hình Microsevice, cloud native app (app được thiết kế chạy trên môi trương cloud)
### Cấu trúc kubernets

- Sơ đồ mô hình kubernets

![mô hình kubernets](images/architecture-K8S.jpg)

- Mô tả `Master`:
    - `Apiserver` đóng vai trò như một API server, cung cấp truy cập đến Kubernets API nơi mà các Kubernets Resource được pushlish.
    - `Sheduler` đảm bảo trách nhiệm cấp phát các Pod ở các Node dự trên các tiêu chí nhất định, các tiêu chí có thể là độ sẵn sàng của tài nguyên.
    - `controller manager` chạy các controller để giao tiếp với các nhà cung cấp dịch vụ như GCP, AWS...
    - `Etcd` là kho lưu trữ key-value phân tabs. etcd trong một Kubetnets Cluster khá quan trọng, được ví như trái tim của kubernets, toàn bộ dữ liệu của Cluster được lưu trữ ở đây (giống như Database của Kubernet Master). Cần sao lưu tốt etcd khi vận hành
- Mô tả `Node`:
    - `Kubelet` chạy trên mỗi node có nhiệm vụ quản lý cac pod ở trên node đó, bao gồm việc kết nối các container như dọcker, giao tiếp với master qua API
    - `kube-proxy` giúp định tuyến rote các kết nối đến Pod chính xác, thực hiện cân bằng tải (loadbalancing) trên các Pod

## Các khái niệm cần nắm trong `Kubernets`
- `Kubernets Resource` Được publish vào Api - Controller xử lý để đưa các cluster vào đúng trạng thái khai báo
    - Các loại Resource quan trọng trong Kubernets:
        - `Pod` là đơn vị thực thi cơ bản nhỏ nhất trong K8s. Pod có thể có một hoặc nhiều Containers.
        ![Pod](images/pod-trong-kubernetes.png)
            - `Aplication` Đây là core container của một ứng dụng, một Pod phải có loại Container này, các mô hình Pod phổ biến thường chỉ có Container.
            - `SideCar` Pod có thể có container loại Sidecar làm một số việc hữu ích để hỗ trợ cho application container. Thường đảm nhiệm vai trò thu thập log, làm `reverse proxy`. Một pod có thể có rất nhiều sidecar container.
            - `Init` đôi khi cần phải có những khởi tạo trước khi khởi chạy Application, Một pod có thể có nhiều init container, chúng có thể chạy theo thứ tự.
        - `Deployment và ReplicaSet`

            ![deployment-trong-kubernetes](images/deployment-trong-kubernetes.png)

            - `Deployment` trong K8s cho phép quản lý vòng đời của các Pod và một Resource liên quan gọi là `ReplicaSet`. Nó cho phép cập nhập một ứng dụng đang chạy mà không xảy ra dowtime. Có tích hợp stratecy để khởi động lại các Pod khi chúng bị die or crash.
            - `ReplicaSet` được tạo khi deployment được tạo hoặc chỉnh sửa, ReplicaSet được dùng như định nghĩa để tạo Pod.
        
        - `DaemonSet` đảm bảo một pod chạy trên tất cả hặc tập hợp nhỏ Kubernets Node đã sẵn sàng. Ngay khi Node mới được thêm vào Cluster, thì Pod sẽ được thêm ngay vào

        - `Stateful Set` quản lý tập hợp các Pod về triển khai và mở rộng. Quản lý đảm bảo về thứ tự sắp xếp và duy nhất của cá Pod.

        - `Job` tạo một hoặc nhiều các Pod đảm bảo rằng ít nhất một lượng nhất định trong số chúng chạy đến khi nào hoàn thành. ngay khi tiến trình thành công thì container sẽ không được khởi động lại
        
        - `Cronjob` đơn giản là để chạỵ job theo lịch định sẵn. -> có thể sử dụng để sao lưu dữ liệu định 


- `Label` là cặp key-value gắn vào một kubernets resource 
 


## Cài đặt ứng dụng



## Thực hành với server cloud GCP